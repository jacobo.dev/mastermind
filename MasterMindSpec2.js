class MasterMind2 {
    constructor(codeGoal) {
        this.codeGoal = codeGoal
    } 

    compare(codeGuess) {
        let pins = {wellPlaced: 0, misplaced: 0}
        let tempGoal = this.codeGoal
        
        // Calculate well placed pins
        tempGoal.forEach((colour, index) => {
            if (colour === codeGuess[index]) {
                pins.wellPlaced += 1
                codeGuess[index] = null
                tempGoal[index] = null
            }            
        });
        
        // Calculate misplaced pins
        tempGoal.forEach((colourGoal, index) => {
            if (colourGoal !== null) {
                codeGuess.forEach((colourGuess, index2) => {
                    if (tempGoal[index] !== null && colourGoal === colourGuess) {
                        pins.misplaced += 1
                        tempGoal[index] = null
                        codeGuess[index2] = null
                    }
                });
            }
        });

        return pins
    }
}

describe("MasterMind2", () => {
    it("Returns full of wellPlaced pins when every colour is correct", () => {
        // Arrange
        const codeGoal = ['BLUE', 'BLUE', 'BLUE', 'BLUE']
        const codeGuess = ['BLUE', 'BLUE', 'BLUE', 'BLUE']
        const game = new MasterMind2(codeGoal)

        // Act
        const pins = game.compare(codeGuess)

        // Asert
        expect(pins).toBe({wellPlaced: 4, misplaced: 0})
    })

    it("Returns 2 well placed and 2 misplaced", () => {
        const codeGoal = ['BLUE', 'BLUE', 'RED', 'YELLOW']
        const codeGuess = ['BLUE', 'BLUE', 'YELLOW', 'RED']
        const game = new MasterMind2(codeGoal)

        const pins = game.compare(codeGuess)

        expect(pins).toBe({wellPlaced: 2, misplaced: 2})
    })

    it("test jacobo", () => {
        const codeGoal = ['BLUE', 'YELLOW', 'YELLOW']
        const codeGuess = ['BLUE', 'BLUE', 'RED']
        const game = new MasterMind2(codeGoal)

        const pins = game.compare(codeGuess)

        expect(pins).toBe({wellPlaced: 1, misplaced: 0})
    })

    it("test sairo", () => {
        const codeGoal = ['YELLOW', 'BLUE', 'YELLOW']
        const codeGuess = ['BLUE', 'BLUE', 'RED']
        const game = new MasterMind2(codeGoal)

        const pins = game.compare(codeGuess)

        expect(pins).toBe({wellPlaced: 1, misplaced: 0})
    })

    it("test zero", () => {
        const codeGoal = ['YELLOW', 'BLUE', 'RED', 'GREEN']
        const codeGuess = ['BLUE', 'YELLOW', 'GREEN', 'RED']
        const game = new MasterMind2(codeGoal)

        const pins = game.compare(codeGuess)

        expect(pins).toBe({wellPlaced: 0, misplaced: 4})
    })

    it("test 6", () => {
        const codeGoal = ['YELLOW', 'BLUE', 'RED', 'BLUE']
        const codeGuess = ['BLUE', 'BLUE', 'BLUE', 'RED']
        const game = new MasterMind2(codeGoal)

        const pins = game.compare(codeGuess)

        expect(pins).toBe({wellPlaced: 1, misplaced: 2})
    })

    it("test 7", () => {
        const codeGoal = ['BLUE', 'RED', 'YELLOW', 'GREEN']
        const codeGuess = ['BROWN', 'BLUE', 'BROWN', 'GREEN']
        const game = new MasterMind2(codeGoal)

        const pins = game.compare(codeGuess)

        expect(pins).toBe({wellPlaced: 1, misplaced: 1})
    })
})